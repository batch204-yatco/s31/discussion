//Use the "require" directive to load Node.js modules
//"http module" - lets Node.js transfer data using Hyper Text Transfer Protocol
// "http module" - is a set of individual files that contain codes to create a component that helps establish data transfer between applications
// HTTP is a protocol that allows fetching of resources such as HTML documents

let http = require("http");

// The http module has a createServer() method that accepts a function as an argument and allows for creation of a server
http.createServer(function (request, response) {

	response.writeHead(200, {'Content-Type': 'text/plain'});
	response.end('Hello World!');

}).listen(4000)

// A port is a virtual point where network connections start and end
// The server will be assigned to port "4000" via the "listen(4000)" method where the server will listen to any request that are sent to it eventually communicating with our server 
// 200 is a successful response

console.log("Server running at localhost: 4000");

// ctrl + c in gitbash to close server

// Start node in server
// nodemon routes.js

// Install auto update in terminal
// npm install -g nodemon 