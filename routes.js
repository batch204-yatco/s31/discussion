const http = require('http');

// Creates variable "port" to store port number 
const port = 4000;

// Creates variable "server" that stores output of "createServer" method
const server = http.createServer((req, res) => {

	if(req.url == '/greeting') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("Hello from the other side")
	}

	else if (req.url == '/home') {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end("This is the homepage");
	}
	
	else {
		res.writeHead(404, {'Content-Type': 'text/plain'})
		res.end("Page not available");
	}
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);

